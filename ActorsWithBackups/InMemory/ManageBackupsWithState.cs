using ActorsWithBackups.Interfaces;

namespace ActorsWithBackups.InMemory
{
    public class ManageBackupsWithState<T, TId> : Base.State.ManageBackupsWithState<T, TId>
    {
        public ManageBackupsWithState(ICanUpdateState<T, TId> myStateUpdater) : base(myStateUpdater)
        {
        }
    }
}