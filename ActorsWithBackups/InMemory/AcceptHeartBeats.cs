namespace ActorsWithBackups.InMemory
{
    public class AcceptHeartBeats<TId> : Base.HeartBeat.AcceptHeartBeats<TId>
    {
        public AcceptHeartBeats(TId id) : base(id)
        {
        }
    }
}