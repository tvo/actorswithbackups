using System.Threading.Tasks;

namespace ActorsWithBackups.InMemory
{
    public class StateUpdater<T, TId> : Base.State.StateUpdater<T, TId>
    {
        public StateUpdater(TId id) : base(id) { }

        private T _state { get; set; }
        public override Task<T> GetState()
        {
            return Task.FromResult(_state);
        }

        protected override Task _SetState(T state)
        {
            _state = state;
            return Task.CompletedTask;
        }
    }
}