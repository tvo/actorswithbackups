using System.Threading.Tasks;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Interfaces
{
    public interface ICanUpdateState<T, TId> : IHaveId<TId>
    {
        Task SetState(StateChangeRequest<T> stateChangeRequest);
        Task<T> GetState();
    }
}