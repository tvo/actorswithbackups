using Immutability;

namespace ActorsWithBackups.Interfaces
{
    [Immutable]
    public interface IHaveId<TId>
    {
        TId Id { get; }
    }
}