using System.Threading.Tasks;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Interfaces
{
    public interface IMonitorWithHeartBeats<TId> : IHaveId<TId>
    {
        Task SendHeartBeat(IAcceptHeartBeats<TId> requester);
        Task ResponseFromHeartBeat(HeartBeatResponse<TId> heartBeatResponse);
    }
}