using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Interfaces
{
    /// I have peers adds extroversion to the introvert peer
    public interface IHavePeers<TId> : IPeer<TId>, IMonitorWithHeartBeats<TId>
    {
        Task SendHeartBeatsToAllPeers();

        Task GossipWithPeers_AboutNewPeer(IPeer<TId> peer);
        Task GossipWithPeers_AboutPeerRemoval(IHaveId<TId> peer);

        // similar to heartbeat, but the purpose is not to monitor if the peer is alive, but to determine if everybody is "on the same page"
        Task GossipWithPeers_BroadcastPeerSocialization(); 
    }
}