using System.Collections.Generic;
using System.Threading.Tasks;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Interfaces
{
    public interface IPeer<TId> : IAcceptHeartBeats<TId>, IHaveId<TId> // peer by itself is an introvert. Does not initiate friendships with other peers.
    {
        IEnumerable<IPeer<TId>> Peers { get; }
        Task NewPeer(IPeer<TId> peer);
        Task RemovePeer(IHaveId<TId> peerId);
        Task RemovePeer(TId peerId);
        Task AcceptGossip(PeerGossip<TId> gossip);
    }
}