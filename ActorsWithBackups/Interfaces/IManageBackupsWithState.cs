using System.Threading.Tasks;

namespace ActorsWithBackups.Interfaces
{
    public interface IManageBackupsWithState<T, TId> : ICanUpdateState<T, TId>
    {
        Task BackupChildren();
        Task RegisterBackup(ICanUpdateState<T, TId> backup);
        Task RemoveBackup(ICanUpdateState<T, TId> backup);
    }
}