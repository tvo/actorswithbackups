using System.Threading.Tasks;

namespace ActorsWithBackups.Interfaces
{
    public interface IReportCollector<TSomething>
    {
        Task CollectReport(TSomething somethingToReported);
    }
}