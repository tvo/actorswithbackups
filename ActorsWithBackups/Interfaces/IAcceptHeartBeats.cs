using System.Threading.Tasks;
using ActorsWithBackups.Messages;
using Immutability;

namespace ActorsWithBackups.Interfaces
{
    public interface IAcceptHeartBeats<TId> : IHaveId<TId>
    {
        Task RespondToHeartBeat(IMonitorWithHeartBeats<TId> sender, HeartBeatRequest heartBeat);
    }
}