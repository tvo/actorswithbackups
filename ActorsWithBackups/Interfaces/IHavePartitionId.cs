using Immutability;

namespace ActorsWithBackups.Interfaces
{
    [Immutable]
    public interface IHavePartitionId<TId>
    {
        TId Id { get; }
    }
}