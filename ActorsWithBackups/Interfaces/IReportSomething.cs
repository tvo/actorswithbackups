using System.Threading.Tasks;

namespace ActorsWithBackups.Interfaces
{
    public interface IReportSomething<TSomething>
    {
        Task Report(TSomething somethingToReport);
    }
}