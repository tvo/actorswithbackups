using System;
using System.Collections.Immutable;
using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;
using Akka.Actor;

namespace ActorsWithBackups.Actors.HeartBeat
{
    public class HeartBeatActor<TId> : ReceiveActor, IAcceptHeartBeats<TId>
    {
        public HeartBeatActor()
        {
            Receive<TId>(idMessage => Id = idMessage);
            Receive<HeartBeatRequest>(message =>
            {
                Sender.Tell(new HeartBeatResponse<TId>(this, message));
            });
        }

        public TId Id { get; set; }

        public Task RespondToHeartBeat(IMonitorWithHeartBeats<TId> sender, HeartBeatRequest heartBeat)
        {
            throw new NotImplementedException();
        }
    }
}