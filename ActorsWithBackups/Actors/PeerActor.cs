using System;
using System.Collections.Immutable;
using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;
using Akka.Actor;

using StateEnum = ActorsWithBackups.Actors.PeerActorStateDescriptors.ActorStates;
using IState = ActorsWithBackups.Actors.PeerActorStateDescriptors.IMyState;

namespace ActorsWithBackups.Actors
{
    public static class PeerActorStateDescriptors
    {
        public enum ActorStates
        {
            Uninitialized,
            Working,
            Ready,
        }

        public interface IMyState { }
    }

    public class PeerActor<T, TId> : FSM<StateEnum, IState>, IWithUnboundedStash where T : IHaveId<TId>
    {

        public IStash Stash { get; set; }

        public PeerActor()
        {
            ActorsWithBackups.InMemory.StateUpdater<T, TId> stateStore = null;

            StartWith(StateEnum.Uninitialized, Uninitialized.Instance);

            When(StateEnum.Uninitialized, state =>
            {
                if (state.FsmEvent is StateChangeRequest<T> message && state.StateData is Uninitialized)
                {
                    stateStore = new ActorsWithBackups.InMemory.StateUpdater<T, TId>(message.State.Id);

                    // start the setting state task.
                    var setStateTask = stateStore.SetState(message);

                    // setup the message to self when the task finishes.
                    var self = Self;
                    setStateTask.ContinueWith(x => self.Tell(Working.WorkFinished.Instance));

                    // transition to the active state.
                    return GoTo(StateEnum.Working).Using(new Working(setStateTask));
                }

                return null;
            });

            When(StateEnum.Working, state =>
            {
                if (state.FsmEvent is Working.WorkFinished)
                {
                    Stash.UnstashAll();
                    return GoTo(StateEnum.Ready).Using(Initialized.Instance);
                }

                Stash.Stash();
                return null;
            });

            When(StateEnum.Ready, state =>
            {
                if (state.FsmEvent is StateChangeRequest<T> stateChangeMessage)
                {
                    var setStateTask = stateStore.SetState(stateChangeMessage);
                    var self = Self;
                    setStateTask.ContinueWith(tsk => self.Tell(Working.WorkFinished.Instance));
                    return GoTo(StateEnum.Working).Using(new Working(setStateTask));
                }
                else if (state.FsmEvent is StateGetRequest<T>)
                {
                    var getStateTask = stateStore.GetState();
                    var sender = Sender; // save the context of the sender.
                    var self = Self;
                    getStateTask.ContinueWith(tsk =>
                    {
                        sender.Tell(tsk.Result);
                        self.Tell(Working.WorkFinished.Instance);
                    });
                    return GoTo(StateEnum.Working).Using(new Working(getStateTask));
                }

                return null;
            });
        }

        #region State classes
        private class Uninitialized : IState
        {
            public static Uninitialized Instance = new Uninitialized();

            private Uninitialized() { }
        }

        private class Working : IState
        {
            public Working(Task tsk)
            {
                this.tsk = tsk;
            }
            public Task tsk { get; }

            public class WorkFinished
            {
                public static WorkFinished Instance = new WorkFinished();

                private WorkFinished() { }
            }
        }

        private class Initialized : IState
        {
            public static Initialized Instance = new Initialized();

            private Initialized() { }
        }
        #endregion
    }
}