namespace ActorsWithBackups.Actors.StateClasses
{
    public enum StateEnum
    {
        Uninitialized,
        Working,
        Ready,
    }
}