using System.Threading.Tasks;

namespace ActorsWithBackups.Actors.StateClasses.States
{
    public class Working : IState
    {
        public Working(Task tsk)
        {
            this.tsk = tsk;
        }
        public Task tsk { get; }

        public class WorkFinished
        {
            public static WorkFinished Instance = new WorkFinished();

            private WorkFinished() { }
        }
    }
}