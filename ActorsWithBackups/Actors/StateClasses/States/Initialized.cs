namespace ActorsWithBackups.Actors.StateClasses.States
{
    public class Initialized : IState
    {
        public static Initialized Instance = new Initialized();

        private Initialized() { }
    }
}