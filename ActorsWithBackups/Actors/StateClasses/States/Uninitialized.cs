namespace ActorsWithBackups.Actors.StateClasses.States
{
    public class Uninitialized : IState
    {
        public static Uninitialized Instance = new Uninitialized();

        private Uninitialized() { }
    }
}