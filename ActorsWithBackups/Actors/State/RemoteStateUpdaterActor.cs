using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;
using Akka.Actor;

namespace ActorsWithBackups.Actors
{
    public class RemoteStateUpdaterActor<T, TId> : ICanUpdateState<T, TId>
        where T : IHaveId<TId>
    {
        private ICanTell Remote { get; }
        private ActorSystem System { get; }
        public IActorRef From { get; set; } = Nobody.Instance;

        public RemoteStateUpdaterActor(ICanTell remote, ActorSystem system)
        {
            Remote = remote;
            System = system;
        }

        public TId Id { get; set; }

        public async Task<T> GetState()
        {
            var inbox = Inbox.Create(System);
            Remote.Tell(new StateGetRequest<T>(), inbox.Receiver);
            var response = await inbox.ReceiveAsync();
            if(response is T result)
                return result;
            throw new System.NotImplementedException();
        }

        public async Task SetState(StateChangeRequest<T> stateChangeRequest)
        {
            Id = stateChangeRequest.State.Id;
            Remote.Tell(stateChangeRequest, From);
            // await GetState(); // await till state is saved.
            await Task.CompletedTask;
        }
    }
}