using System;
using System.Threading.Tasks;
using ActorsWithBackups.Actors.StateClasses;
using ActorsWithBackups.Actors.StateClasses.States;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;
using Akka.Actor;

namespace ActorsWithBackups.Actors.State
{
    public class ManageBackupsWithState<T, TId> : FSM<StateEnum, IState>, IManageBackupsWithState<T, TId>, IWithUnboundedStash
    {
        public TId Id { get; private set; }
        private IManageBackupsWithState<T, TId> stateStore { get; }
        public IStash Stash { get; set; }

        public ManageBackupsWithState(IManageBackupsWithState<T, TId> store)
        {
            stateStore = store ?? throw new ArgumentNullException(nameof(store));
            StartWith(StateEnum.Uninitialized, Uninitialized.Instance);

            When(StateEnum.Uninitialized, state =>
            {
                if (state.FsmEvent is StateChangeRequest<T> message && state.StateData is Uninitialized)
                {
                    // start the setting state task.
                    var setStateTask = this.SetState(message);

                    // setup the message to self when the task finishes.
                    var self = Self;
                    setStateTask.ContinueWith(x => self.Tell(Working.WorkFinished.Instance));

                    // transition to the active state.
                    return GoTo(StateEnum.Working).Using(new Working(setStateTask));
                }

                return null;
            });

            When(StateEnum.Working, state =>
            {
                if (state.FsmEvent is Working.WorkFinished)
                {
                    Stash.UnstashAll();
                    return GoTo(StateEnum.Ready).Using(Initialized.Instance);
                }

                Stash.Stash();
                return null;
            });

            When(StateEnum.Ready, state =>
            {
                if (state.FsmEvent is StateChangeRequest<T> stateChangeMessage)
                {
                    // TODO: check for a bad id.
                    var setStateTask = SetState(stateChangeMessage);
                    var self = Self;
                    setStateTask.ContinueWith(tsk => self.Tell(Working.WorkFinished.Instance));
                    return GoTo(StateEnum.Working).Using(new Working(setStateTask));
                }
                else if (state.FsmEvent is StateGetRequest<T>)
                {
                    var getStateTask = stateStore.GetState();
                    var sender = Sender; // save the context of the sender.
                    var self = Self;
                    getStateTask.ContinueWith(tsk =>
                    {
                        sender.Tell(tsk.Result);
                        self.Tell(Working.WorkFinished.Instance);
                    });
                    return GoTo(StateEnum.Working).Using(new Working(getStateTask));
                }

                return null;
            });
        }

        public Task BackupChildren()
        {
            throw new System.NotImplementedException();
        }

        public Task<T> GetState()
        {
            throw new System.NotImplementedException();
        }

        public Task RegisterBackup(ICanUpdateState<T, TId> backup)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveBackup(ICanUpdateState<T, TId> backup)
        {
            throw new System.NotImplementedException();
        }

        public Task SetState(StateChangeRequest<T> stateChangeRequest)
        {
            throw new System.NotImplementedException();
        }
    }
}