using Immutability;

namespace ActorsWithBackups.Messages
{
    public class StateChangeRequest<T>
    {
        public StateChangeRequest(T state)
        {
            State = state;
        }

        public long TimeRequested { get; } = System.DateTimeOffset.UtcNow.UtcTicks;
        public T State { get; }
    }
}