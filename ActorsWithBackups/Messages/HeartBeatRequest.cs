using System;
using Immutability;

namespace ActorsWithBackups.Messages
{
    [Immutable]
    public class HeartBeatRequest
    {
        private static Random _Random = new Random();
        public int Nonce { get; } = _Random.Next();
    }
}