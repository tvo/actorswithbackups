using ActorsWithBackups.Interfaces;

namespace ActorsWithBackups.Messages
{
    public abstract class IdBaseClass<TId> : IHaveId<TId>
    {
        protected IdBaseClass(TId id)
        {
            Id = id;
        }
        
        public TId Id { get; }
    }
}