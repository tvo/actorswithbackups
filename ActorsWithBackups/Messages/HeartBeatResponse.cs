using ActorsWithBackups.Interfaces;
using Immutability;

namespace ActorsWithBackups.Messages
{
    [Immutable]
    public class HeartBeatResponse<TId>
    {
        public HeartBeatResponse(IAcceptHeartBeats<TId> me, HeartBeatRequest request) : this(me.Id, request.Nonce) { }
        public HeartBeatResponse(TId myId, int nonce)
        {
            SenderId = myId;
            Nonce = nonce;
        }

        public TId SenderId { get; }
        public int Nonce { get; }
    }
}