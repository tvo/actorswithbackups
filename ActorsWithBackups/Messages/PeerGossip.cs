using System.Collections.Generic;
using System.Linq;
using ActorsWithBackups.Interfaces;
using Immutability;

namespace ActorsWithBackups.Messages
{
    [Immutable]
    public class PeerGossip<TId>
    {
        public PeerGossip(IEnumerable<IPeer<TId>> peers)
        {
            PeerList = peers.ToList().AsReadOnly();
        }
        
        public IReadOnlyCollection<IPeer<TId>> PeerList { get; }
    }
}