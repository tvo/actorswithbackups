using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Base.HeartBeat
{
    public class AcceptHeartBeats<TId> : IAcceptHeartBeats<TId>
    {
        public AcceptHeartBeats(TId id)
        {
            Id = id;
        }

        public TId Id { get; }

        public virtual async Task RespondToHeartBeat(IMonitorWithHeartBeats<TId> sender, HeartBeatRequest heartBeat)
        {
            await sender.ResponseFromHeartBeat(new HeartBeatResponse<TId>(this, heartBeat));
        }
    }
}