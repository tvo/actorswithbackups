using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Base.HeartBeat
{
    public class MonitorWithHeartBeats<TId> : IMonitorWithHeartBeats<TId>
    {
        public MonitorWithHeartBeats(TId id, Func<TId, Task> missedHeartBeatFunc)
        {
            Id = id;
            this.missedHeartBeatFunc = missedHeartBeatFunc;
        }

        public TId Id { get; }
        private Func<TId, Task> missedHeartBeatFunc { get; } // = x => Task.CompletedTask;
        protected readonly List<Tuple<TId, int>> NonceSenderList = new List<Tuple<TId, int>>();

        public virtual Task ResponseFromHeartBeat(HeartBeatResponse<TId> heartBeatResponse)
        {
            var nonceRegister = NonceSenderList.FirstOrDefault(x => x.Item1.Equals(heartBeatResponse.SenderId) && x.Item2 == heartBeatResponse.Nonce);
            if (nonceRegister != null)
            {
                NonceSenderList.Remove(nonceRegister);
                return Task.CompletedTask;
            }

            // handle failed heartbeat message.
            return Task.CompletedTask;
        }

        public virtual async Task SendHeartBeat(IAcceptHeartBeats<TId> recipient)
        {
            // If there is no responce since the last heart beat request, notify somebody.
            if (NonceSenderList.Any(x => x.Item1.Equals(recipient.Id)))
                await missedHeartBeatFunc(recipient.Id);

            var beat = new Messages.HeartBeatRequest();
            NonceSenderList.Add(Tuple.Create(recipient.Id, beat.Nonce));
            await recipient.RespondToHeartBeat(this, beat);
        }
    }
}