using System;
using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;

namespace ActorsWithBackups.Base.Reporters
{
    public class ActionReportCollector<TCollector, TReport> : IReportCollector<TReport>
    {
        public ActionReportCollector(Action<TReport> report)
        {
            Report = report;
        }

        public Action<TReport> Report { get; }

        public Task CollectReport(TReport somethingToReported)
        {
            Report(somethingToReported);
            return Task.CompletedTask;
        }
    }
}