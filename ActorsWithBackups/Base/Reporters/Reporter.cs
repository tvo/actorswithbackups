using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;

namespace ActorsWithBackups.Base.Reporters
{
    public class Reporter<TFromType, TReport> : IReportSomething<TReport>
    {
        public Reporter(IReportCollector<TReport> collector)
        {
            Collector = collector;
        }

        private IReportCollector<TReport> Collector { get; }

        public async Task Report(TReport somethingToReport)
        {
            await Collector.CollectReport(somethingToReport);
        }
    }
}