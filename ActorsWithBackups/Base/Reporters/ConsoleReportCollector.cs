using System;
using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;

namespace ActorsWithBackups.Base.Reporters
{
    public class ConsoleReportCollector<TCollector, TReport> : ActionReportCollector<TCollector, TReport>
    {
        public ConsoleReportCollector() : base(x => Console.WriteLine($"{typeof(TCollector).Name} - {x.ToString()}")) { }
    }
}