using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActorsWithBackups.Base.HeartBeat;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Base.Peer
{
    public class Peer<TId> : AcceptHeartBeats<TId>, IPeer<TId>
    {
        public Peer(TId id) : base(id) { }

        public virtual IEnumerable<IPeer<TId>> Peers => PeerList;

        protected List<IPeer<TId>> PeerList = new List<IPeer<TId>>();


        public virtual Task NewPeer(IPeer<TId> peer)
        {
            if (peer != null && !peer.Id.Equals(Id) && !PeerList.Contains(peer))
                PeerList.Add(peer);
            return Task.CompletedTask;
        }

        public virtual Task RemovePeer(IHaveId<TId> peer) => RemovePeer(peer.Id);
        public virtual Task RemovePeer(TId peerId)
        {
            var localPeer = Peers.FirstOrDefault(x => x.Id.Equals(peerId));
            if (localPeer != null)
                PeerList.Remove(localPeer);
            return Task.CompletedTask;
        }

        public async Task AcceptGossip(PeerGossip<TId> gossip)
        {
            var toAddList = gossip.PeerList.Where(x => !Peers.Any(p => x.Id.Equals(p.Id))).ToList();
            var toRemoveList = Peers.Where(p => gossip.PeerList.Any(x => !x.Id.Equals(p.Id))).ToList();

            var taskList = new List<Task>();
            taskList.AddRange(toAddList.Select(x => this.NewPeer(x)));
            await Task.WhenAll(taskList);
        }
    }
}