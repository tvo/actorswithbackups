using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActorsWithBackups.Base.HeartBeat;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Base.Peer
{
    public class SocialPeer<TId> : Peer<TId>, IHavePeers<TId>
    {
        public SocialPeer(TId id) : base(id)
        {
            heartBeatMonitor = new MonitorWithHeartBeats<TId>(id, async pId =>
            {
                var peerToRemove = Peers.FirstOrDefault(x => x.Id.Equals(pId));
                if (peerToRemove is null)
                    return;
                await GossipWithPeers_AboutPeerRemoval(peerToRemove);
            });
        }

        private IMonitorWithHeartBeats<TId> heartBeatMonitor { get; }

        public override async Task NewPeer(IPeer<TId> peer)
        {
            // don't add myself, and don't add duplicates.
            if (peer != null && !Id.Equals(peer.Id) && !Peers.Contains(peer))
            {
                await GossipWithPeers_AboutNewPeer(peer);
            }
        }

        public virtual async Task GossipWithPeers_AboutNewPeer(IPeer<TId> peer)
        {
            // don't add myself, and don't add duplicates.
            if (peer == null || Id.Equals(peer.Id) || Peers.Contains(peer))
                return;

            await base.NewPeer(peer);

            var gossipTaskList = new List<Task>(Peers.Count());
            foreach (var p in Peers)
            {
                if (p.Id.Equals(peer.Id)) // socialize myself with the peer added.
                    gossipTaskList.Add(p.NewPeer(this));
                else
                    gossipTaskList.Add(p.NewPeer(p));
            }
            await Task.WhenAll(gossipTaskList);
        }

        public virtual async Task GossipWithPeers_AboutPeerRemoval(IHaveId<TId> uglyPeer)
        {
            // don't add myself, and don't remove something that isn't there.
            if (uglyPeer == null || Id.Equals(uglyPeer.Id) || !Peers.Any(x => x.Id.Equals(uglyPeer.Id)))
                return;

            await base.RemovePeer(uglyPeer);

            var gossipTaskList = new List<Task>(Peers.Count());
            foreach (var p in Peers)
                gossipTaskList.Add(p.RemovePeer(uglyPeer));
            await Task.WhenAll(gossipTaskList);
        }


        public async Task GossipWithPeers_BroadcastPeerSocialization()
        {
            var gossipTaskList = new List<Task>(Peers.Count());
            var peerList = Peers.ToList();
            peerList.Add(this);
            foreach (var peer in Peers)
                gossipTaskList.Add(peer.AcceptGossip(gossip: new PeerGossip<TId>(peerList)));
            await Task.WhenAll(gossipTaskList);
        }

        public Task ResponseFromHeartBeat(HeartBeatResponse<TId> heartBeatResponse) => heartBeatMonitor.ResponseFromHeartBeat(heartBeatResponse);
        public Task SendHeartBeat(IAcceptHeartBeats<TId> requester) => heartBeatMonitor.SendHeartBeat(requester);

        public virtual async Task SendHeartBeatsToAllPeers()
        {
            await Task.WhenAll(Peers.Select(peer =>
                SendHeartBeat(peer)
            ).ToList());
        }
    }
}