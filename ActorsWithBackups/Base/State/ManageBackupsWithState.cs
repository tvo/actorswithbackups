using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Base.State
{
    public class ManageBackupsWithState<T, TId> : IManageBackupsWithState<T, TId>
    {
        protected ManageBackupsWithState(ICanUpdateState<T, TId> myStateUpdater)
        {
            MyStateUpdater = myStateUpdater;
        }

        public Interfaces.ICanUpdateState<T, TId> MyStateUpdater { get; }

        protected readonly ICollection<ICanUpdateState<T, TId>> Backups = new List<ICanUpdateState<T, TId>>();

        public TId Id => MyStateUpdater.Id;


        // broadcast to the children the current state.
        public async Task BackupChildren()
        {
            var myState = await GetState();
            var message = new Messages.StateChangeRequest<T>(myState);
            var backUpList = Backups.Select(x => x.SetState(message));
            await Task.WhenAll(backUpList);
        }

        public Task<T> GetState() => MyStateUpdater.GetState();

        public Task RegisterBackup(ICanUpdateState<T, TId> backup)
        {
            Backups.Add(backup);
            return Task.CompletedTask;
        }

        public async Task SetState(StateChangeRequest<T> stateChangeRequest)
        {
            await MyStateUpdater.SetState(stateChangeRequest);
            await BackupChildren();
        }

        public Task RemoveBackup(ICanUpdateState<T, TId> backup)
        {
            var b = Backups.Where(x => x.Id.Equals(backup.Id)).FirstOrDefault();
            if (b != null)
                Backups.Remove(b);
            return Task.CompletedTask;
        }
    }
}