using System.Threading.Tasks;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;

namespace ActorsWithBackups.Base.State
{
    public class StateUpdater<T, TId> : IdBaseClass<TId>, ICanUpdateState<T, TId>
    {
        protected StateUpdater(TId id) : base(id) { }

        public Task<T> GetState();
        protected Task _SetState(T state);

        public async Task SetState(StateChangeRequest<T> stateChangeRequest) => await _SetState(stateChangeRequest.State);
    }
}