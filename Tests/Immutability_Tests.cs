using System;
using Xunit;

namespace Tests
{
    public class Immutability_Tests
    {
        [Fact]
        public void EnsureStructsAreImmutableTest()
        {
            Type t = typeof(Immutability_Tests);
            var p = new Type[] { t };
            new Immutability.Test(p).EnsureStructsAreImmutableTest();
        }

        [Fact]
        public void EnsureImmutableTypeFieldsAreMarkedImmutableTest()
        {
            Type t = typeof(Immutability_Tests);
            var p = new Type[] { t };
            new Immutability.Test(p).EnsureImmutableTypeFieldsAreMarkedImmutableTest();
        }

    }
}
