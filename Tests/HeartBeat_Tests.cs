using System;
using System.Threading.Tasks;
using ActorsWithBackups.Base.HeartBeat;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;
using FluentAssertions;
using Moq;
using Xunit;

namespace Tests
{
    public class HeartBeat_Tests
    {
        [Fact]
        public void Id_IsSet()
        {
            //Given
            //When
            var accepter = new AcceptHeartBeats<int>(5);

            //Then
            accepter.Id.Should().Be(5);
        }

        [Fact]
        public async Task AcceptHeartBeats_RespondsToRequest()
        {
            //Given
            var doctorThatChecksHeartBeat = new Mock<IMonitorWithHeartBeats<int>>();
            var personWithHeartBeat = new AcceptHeartBeats<int>(5);

            //When
            await personWithHeartBeat.RespondToHeartBeat(doctorThatChecksHeartBeat.Object, new HeartBeatRequest());

            //Then
            personWithHeartBeat.Id.Should().Be(5);
            doctorThatChecksHeartBeat.Verify(x => x.ResponseFromHeartBeat(It.IsAny<HeartBeatResponse<int>>()));
        }

        [Fact]
        public async Task AcceptHeartBeats_MissedHeartBeat()
        {
            //Given
            var missedFlag = false;
            Func<int, Task> notificationFunc = xId => { missedFlag = true; return Task.CompletedTask; };

            var doctorThatChecksHeartBeat = new MonitorWithHeartBeats<int>(4, notificationFunc);
            var personWithHeartBeat = new Mock<AcceptHeartBeats<int>>(5);
            // setup person to not respond to heart beat request.
            personWithHeartBeat.Setup(x => x.RespondToHeartBeat(It.IsAny<IMonitorWithHeartBeats<int>>(), It.IsAny<HeartBeatRequest>())).Returns(Task.CompletedTask);

            //When
            await doctorThatChecksHeartBeat.SendHeartBeat(personWithHeartBeat.Object);
            missedFlag.Should().BeFalse();
            await doctorThatChecksHeartBeat.SendHeartBeat(personWithHeartBeat.Object);

            //Then
            missedFlag.Should().BeTrue();
        }

        [Fact]
        public async Task AcceptHeartBeats_RespondsToRequest_NotMissed()
        {
            //Given
            var doctorThatChecksHeartBeat = new Mock<IMonitorWithHeartBeats<int>>();
            var personWithHeartBeat = new AcceptHeartBeats<int>(5);

            //When
            await personWithHeartBeat.RespondToHeartBeat(doctorThatChecksHeartBeat.Object, new HeartBeatRequest());
            await personWithHeartBeat.RespondToHeartBeat(doctorThatChecksHeartBeat.Object, new HeartBeatRequest());
            await personWithHeartBeat.RespondToHeartBeat(doctorThatChecksHeartBeat.Object, new HeartBeatRequest());
            await personWithHeartBeat.RespondToHeartBeat(doctorThatChecksHeartBeat.Object, new HeartBeatRequest());
            await personWithHeartBeat.RespondToHeartBeat(doctorThatChecksHeartBeat.Object, new HeartBeatRequest());

            //Then
            personWithHeartBeat.Id.Should().Be(5);
            doctorThatChecksHeartBeat.Verify(x => x.ResponseFromHeartBeat(It.IsAny<HeartBeatResponse<int>>()), Times.Exactly(5));
        }

        [Fact]
        public async Task AcceptHeartBeats_NotMissedHeartBeat_DoesNotNotify()
        {
            //Given
            var missedFlag = false;
            Func<int, Task> notificationFunc = xId => { missedFlag = true; return Task.CompletedTask; };

            var doctorThatChecksHeartBeat = new Mock<MonitorWithHeartBeats<int>>(4, notificationFunc) { CallBase = true };
            var personWithHeartBeat = new Mock<AcceptHeartBeats<int>>(5) { CallBase = true };

            //When
            await doctorThatChecksHeartBeat.Object.SendHeartBeat(personWithHeartBeat.Object);
            await doctorThatChecksHeartBeat.Object.SendHeartBeat(personWithHeartBeat.Object);
            await doctorThatChecksHeartBeat.Object.SendHeartBeat(personWithHeartBeat.Object);
            await doctorThatChecksHeartBeat.Object.SendHeartBeat(personWithHeartBeat.Object);
            await doctorThatChecksHeartBeat.Object.SendHeartBeat(personWithHeartBeat.Object);

            //Then
            doctorThatChecksHeartBeat.Verify(x => x.ResponseFromHeartBeat(It.IsAny<HeartBeatResponse<int>>()), Times.Exactly(5));
            missedFlag.Should().BeFalse();
        }
    }
}