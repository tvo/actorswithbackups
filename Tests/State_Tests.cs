using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using System;
using ActorsWithBackups.InMemory;
using ActorsWithBackups.Messages;

namespace Tests
{
    public class State_Tests
    {
        [Fact]
        public async Task CanSaveState()
        {
            //Given
            var state = "new state";
            var store = new StateUpdater<string, int>(5);

            //When
            await store.SetState(new StateChangeRequest<string>(state));
            var returnedState = await store.GetState();

            //Then
            returnedState.Should().Be(state);
        }

        [Fact]
        public async Task ManageBackupsWithState_CanDistributeState()
        {
            //Given
            var state = "new state";
            var backup = new StateUpdater<string, int>(5);
            var backup2 = new StateUpdater<string, int>(5);
            var primary = new ManageBackupsWithState<string, int>(new StateUpdater<string, int>(5));
            var request = new StateChangeRequest<string>(state);

            //When
            await primary.RegisterBackup(backup);
            await primary.RegisterBackup(backup2);
            await primary.SetState(request);
            var statePrimary = await primary.GetState();
            var stateBackup = await backup.GetState();
            var stateBackup2 = await backup2.GetState();

            //Then
            statePrimary.Should().Be(state);
            stateBackup.Should().Be(state);
            stateBackup2.Should().Be(state);
        }
    }
}