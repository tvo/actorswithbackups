using Xunit;
using FluentAssertions;
using Akka.TestKit.Xunit2;
using ActorsWithBackups.Actors;
using ActorsWithBackups.Interfaces;
using Akka.Actor;
using ActorsWithBackups.Messages;
using System;
using System.Threading.Tasks;

namespace Tests.Actors.State
{
    public class RemoteStateActorTests : TestKit
    {
        class StateClass : IHaveId<string>
        {
            public string Id { get; set; }
        }

        [Fact]
        public void StoreStateInitially()
        {
            // Given
            var probe = this.CreateTestProbe();
            var remoteState = new RemoteStateUpdaterActor<StateClass, string>(probe, Sys);
            var state = new StateClass() { Id = "asdf" };

            // When
            remoteState.SetState(new StateChangeRequest<StateClass>(state)).Wait();

            // Then
            probe.ExpectMsg((StateChangeRequest<StateClass> msg) => msg.State.Id == "asdf");
        }

        [Fact]
        public async Task StoreStateAfterInit()
        {
            // Given
            var probe = this.CreateTestProbe();
            var remoteState = new RemoteStateUpdaterActor<StateClass, string>(probe, Sys);
            var state = new StateClass() { Id = "asdf" };

            // When
            await remoteState.SetState(new StateChangeRequest<StateClass>(state));
            probe.ExpectMsg((StateChangeRequest<StateClass> msg) => true);
            var getTask = remoteState.GetState();
            probe.ExpectMsg((StateGetRequest<StateClass> msg) => true);
            probe.Sender.Tell(state);
            var resultState = await getTask;

            // Then
            resultState.Id.Should().Be("asdf");
        }
    }
}