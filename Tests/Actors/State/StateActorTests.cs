using Xunit;
using FluentAssertions;
using Akka.TestKit.Xunit2;
using ActorsWithBackups.Actors;
using ActorsWithBackups.Interfaces;
using Akka.Actor;
using ActorsWithBackups.Messages;
using System;
using ActorsWithBackups.Base.State;

namespace Tests.Actors.State
{

    public class StateActorTests : TestKit
    {
        class StateClass : IHaveId<string>
        {
            public string Id { get; set; }
        }

        ICanUpdateState<StateClass, string> stateUpdater = new StateUpdater<StateClass, string>("");

        [Fact]
        public void StoreStateInitially()
        {
            // Given
            var stateActor = this.Sys.ActorOf(Props.Create(() => new StateUpdaterActor<StateClass, string>()));
            var state = new StateClass() { Id = "asdf" };

            // When
            stateActor.Tell(new StateChangeRequest<StateClass>(state));
            stateActor.Tell(new StateGetRequest<StateClass>());

            // Then
            ExpectMsg((StateClass msg) => msg.Id == "asdf");
        }

        [Fact]
        public void StoreStateAfterInit()
        {
            // Given
            var stateActor = this.Sys.ActorOf(Props.Create(() => new StateUpdaterActor<StateClass, string>()));

            // When
            stateActor.Tell(new StateChangeRequest<StateClass>(new StateClass() { Id = "one" }));
            stateActor.Tell(new StateChangeRequest<StateClass>(new StateClass() { Id = "two" }));
            stateActor.Tell(new StateGetRequest<StateClass>());

            // Then
            ExpectMsg((StateClass msg) => msg.Id == "two");
        }
    }
}