using System.Threading.Tasks;
using Xunit;
using ActorsWithBackups.Base.Peer;
using FluentAssertions;
using System.Linq;
using Moq;
using ActorsWithBackups.Interfaces;
using ActorsWithBackups.Messages;

namespace Tests.Peer_Tests
{
    public class Social_Peer_Tests
    {
        [Fact]
        public async Task AddPeer_StartGossip_Peer()
        {
            // Given
            var peer = new SocialPeer<int>(4);
            var friend = new Mock<SocialPeer<int>>(5) { CallBase = true };

            // When
            await peer.NewPeer(friend.Object);

            // Then
            friend.Verify(x => x.GossipWithPeers_AboutNewPeer(It.IsAny<IPeer<int>>()), Times.Once);
            peer.Peers.Single().Should().Be(friend.Object);
            friend.Object.Peers.Single().Should().Be(peer);
        }

        [Fact]
        public async Task AddPeerViaGossip_AddGossip_Peer()
        {
            // Given
            var peer = new SocialPeer<int>(4);
            var friend = new Mock<SocialPeer<int>>(5) { CallBase = true };

            // When
            await peer.GossipWithPeers_AboutNewPeer(friend.Object);

            // Then
            friend.Verify(x => x.GossipWithPeers_AboutNewPeer(It.IsAny<IPeer<int>>()), Times.Once);
            peer.Peers.Single().Should().Be(friend.Object);
            friend.Object.Peers.Single().Should().Be(peer);
        }

        [Fact]
        public async Task AddPeerViaGossip_StartGossip_Peer()
        {
            // Given
            var peer = new Mock<SocialPeer<int>>(4) { CallBase = true };
            var friend = new SocialPeer<int>(5);
            peer.Setup(x => x.Peers).Returns(new[] { friend });

            // When
            await peer.Object.GossipWithPeers_BroadcastPeerSocialization();

            // Then
            friend.Peers.Should().ContainSingle();
            friend.Peers.Single().Should().Be(peer.Object);
        }

        [Fact]
        public async Task RemovePeer_ByRemove_Peer()
        {
            // Given
            var peer = new SocialPeer<int>(4);
            var friend = new SocialPeer<int>(5);
            await peer.GossipWithPeers_AboutNewPeer(friend);

            // When
            await peer.RemovePeer(friend);

            // Then
            peer.Peers.Should().BeEmpty();
            friend.Peers.Should().ContainSingle();
        }

        [Fact]
        public async Task RemovePeer_ByGossip_Peer()
        {
            // Given
            var peer = new SocialPeer<int>(4);
            var friend = new SocialPeer<int>(5);
            await peer.GossipWithPeers_AboutNewPeer(friend);

            // When
            await peer.GossipWithPeers_AboutPeerRemoval(friend);

            // Then
            peer.Peers.Should().BeEmpty();
            friend.Peers.Should().ContainSingle();
        }

        [Fact]
        public async Task RemovePeer_ByFailedHeartBeat()
        {
            // Given
            var peer = new SocialPeer<int>(4);
            var friendWhoDoesNotAnswerPhone = new Mock<SocialPeer<int>>(5) { CallBase = true };
            await peer.GossipWithPeers_AboutNewPeer(friendWhoDoesNotAnswerPhone.Object);
            friendWhoDoesNotAnswerPhone.Setup(x => x.RespondToHeartBeat(It.IsAny<IMonitorWithHeartBeats<int>>(), It.IsAny<HeartBeatRequest>()))
                .Returns(Task.CompletedTask);

            // When
            await peer.SendHeartBeatsToAllPeers();
            await peer.SendHeartBeatsToAllPeers();

            // Then
            peer.Peers.Should().BeEmpty();
        }

        [Fact]
        public async Task SuccessfulHeartbeats_DoesNotRemovePeer()
        {
            // Given
            var peer = new SocialPeer<int>(4);
            var friendWhoDoesNotAnswerPhone = new SocialPeer<int>(5);
            await peer.GossipWithPeers_AboutNewPeer(friendWhoDoesNotAnswerPhone);

            // When
            await peer.SendHeartBeatsToAllPeers();
            await peer.SendHeartBeatsToAllPeers();

            // Then
            peer.Peers.Should().NotBeEmpty();
        }
    }
}