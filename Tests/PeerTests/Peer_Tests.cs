using System.Threading.Tasks;
using Xunit;
using ActorsWithBackups.Base.Peer;
using FluentAssertions;
using System.Linq;
using ActorsWithBackups.Messages;

namespace Tests.Peer_Tests
{
    public class Peer_Tests
    {

        [Fact]
        public async Task AddPeer_IsAdded()
        {
            // Given
            var peer = new Peer<int>(4);
            var friend = new Peer<int>(5);

            // When
            await peer.NewPeer(friend);

            // Then
            peer.Peers.Single().Should().Be(friend);
        }

        [Fact]
        public async Task AddPeer_IsAddedThenRemoved()
        {
            // Given
            var peer = new Peer<int>(4);
            var friend = new Peer<int>(5);
            await peer.NewPeer(friend);

            // When
            await peer.RemovePeer(5);

            // Then
            peer.Peers.Should().BeEmpty();
        }

        [Fact]
        public async Task Peer_AcceptsGossip()
        {
            // Given
            var peer = new Peer<int>(4);
            var friend = new Peer<int>(5);

            // When
            await peer.AcceptGossip(new PeerGossip<int>(new[] { friend }));

            // Then
            peer.Peers.Should().ContainSingle();
        }
    }
}