using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using System;
using ActorsWithBackups.Base.Reporters;

namespace Tests
{
    public class Reporter_Tests
    {
        [Fact]
        public async Task ReportSomething()
        {
            //Given
            string report = "";
            Action<string> reportFunc = x => report = x;
            var reportCollector = new ActionReportCollector<int, string>(reportFunc);
            var reporter = new Reporter<int, string>(reportCollector);
            var memo = "the building is on fire!";

            //When
            await reporter.Report(memo);
            
            //Then
            report.Should().Be(memo);
        }
    }
}